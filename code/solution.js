/**
 * Created by desarrollo on 6/14/16.
 */

var pubnub = require('../config/config.js');

pubnub.subscribe({
    channel: 'optimiza_chile',
    callback: function(m){
        console.log(m);
    },
    error: function(e){
        console.log(e);
    },
    message: function(m){
        console.log(m.replace(/[^\w\s]/gi, ''))
    },
    connect: function(c){
        console.log(c);
    }
});

var test = pubnub.publish({
    channel: 'optimiza_chile',
    message: {"title":"Un Chiste", "text": "∙%$&∙&%%/&&((/%&/%&/%&/%&/Romeo y Julieta se conocieron en línea en el chat, pero su relación tuvo un final trágico, cuando a Julieta se le cayó la red.&/($/%$&∙$&%$/&/(//&((/%&&%$/∙%∙&&%∙&"}
});